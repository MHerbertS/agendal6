@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Exibição dos dados do Contato</h1>
@stop

@section('content')
<div class="panel panel-default">

        <div class=panel"panel-heading">
            Exibe oos dados do contato
        </div>
            
        <div class = "panel-body">
            <table class="table table-bordered table-hover table-striped">
                <tbody>
                
                    <tr>
                    
                        <td class="col-md-2"> ID </td>
                        <td> {{ $agenda->id}} </td>

                    </tr>
                    
                    <tr>
                    
                        <td class="col-md-2"> name </td>
                        <td> {{ $agenda->name}} </td>

                    </tr>
                    
                    <tr>
                    
                        <td class="col-md-2"> fone_res </td>
                        <td> {{ $agenda->fone_res}} </td>

                    </tr>
                
                    <tr>
                    
                        <td class="col-md-2"> fone_cel </td>
                        <td> {{ $agenda->fone_cel}} </td>

                    </tr>

                    <tr>
                    
                        <td class="col-md-2"> dt_nasc </td>
                        <td class="col-md-10"> {{ date ('d/m/Y', strtotime ($agenda->dt_nasc))}} </td>

                    </tr>

                    <tr>
                    
                        <td class="col-md-2"> email </td>
                        <td> {{ $agenda->email}} </td>

                    </tr>

                    <tr>
                    
                        <td class="col-md-2"> facebook </td>
                        <td> {{ $agenda->facebook}} </td>

                    </tr>

                    <tr>
                    
                        <td class="col-md-2"> twitter </td>
                        <td> {{ $agenda->twitter}} </td>

                    </tr>

                    <tr>
                    
                        <td class="col-md-2"> instagram </td>
                        <td> {{ $agenda->instagram}} </td>

                    </tr>
                </tbody>
            </table>
        </div>

        <div class "panel-footer">
                <a href="{{route('agenda.index')}}"class="btn btn-default">
                    <i class "fas fa-reply"></i> Voltar
                </a>
        </div>

</div>
@stop

@section('css')
@stop

@section('js')
@stop