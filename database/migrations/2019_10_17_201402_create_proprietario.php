<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProprietario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietario', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('Nome', 70)->nullable();	
            $table->string('RG', 70)->nullable();	
            $table->string('CPF', 70)->nullable();	
            $table->string('fone_res', 15)->nullable();
            $table->string('fone_cel', 15);	
            $table->date('dt_nasc')->nullable();
            $table->string('email', 50)->nullable();	
            $table->string('facebook', 70)->nullable();	
            $table->string('twitter', 70)->nullable();	
            $table->string('instagram', 70)->nullable();	
            $table->string('Comprovante_de_titulo_proprietario', 15)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proprietario');
    }
}
