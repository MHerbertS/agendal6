<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('Tipo_imovel', 70)->nullable();
            $table->string('CEP', 70)->nullable();
            $table->string('Endereço', 15)->nullable();
            $table->string('Numero', 70)->nullable();
            $table->string('Quartos', 70)->nullable();
            $table->string('Suítes', 70);
            $table->string('Vagas_de_garagem', 70);
            $table->string('Banheiros ', 70)->nullable();
            $table->string('Área_total (M²)', 70);
            $table->string('Área_útil (M²)', 70)->nullable();
            $table->string('Descrição', 70);
            $table->string('Valor', 70)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imoveis');
    }
}
