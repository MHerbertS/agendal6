<?php

namespace App\Http\Controllers;

use App\imoveis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImoveisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\imoveis  $imoveis
     * @return \Illuminate\Http\Response
     */
    public function show(imoveis $imoveis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\imoveis  $imoveis
     * @return \Illuminate\Http\Response
     */
    public function edit(imoveis $imoveis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\imoveis  $imoveis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, imoveis $imoveis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\imoveis  $imoveis
     * @return \Illuminate\Http\Response
     */
    public function destroy(imoveis $imoveis)
    {
        //
    }
}
