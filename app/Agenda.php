<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = "agendas";

    protected $fillabe = [
        'nome',
        'fone_res',
        'fone_Cel',
        'dt_nasc',
        'facebook',
        'twitter',
        'instagram',
        'email'
    ];
    

}
